# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Kristóf Kiszel <ulysses@kubuntu.org>, 2014, 2015, 2019.
# Kiszel Kristóf <kiszel.kristof@gmail.com>, 2017, 2018, 2020.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2020-12-18 02:39+0100\n"
"PO-Revision-Date: 2020-12-25 11:40+0100\n"
"Last-Translator: Kristóf Kiszel <kiszel.kristof@gmail.com>\n"
"Language-Team: Hungarian <kde-l10n-hu@kde.org>\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 21.03.70\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Kiszel Kristóf"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "kiszel.kristof@gmail.com"

#: kcm.cpp:55
#, kde-format
msgid "Screen Locking"
msgstr "Képernyőzárolás"

#: kcm.cpp:57
#, kde-format
msgid "Martin Gräßlin"
msgstr "Martin Gräßlin"

#: kcm.cpp:58
#, kde-format
msgid "Kevin Ottens"
msgstr "Kevin Ottens"

#: package/contents/ui/Appearance.qml:30
#, kde-format
msgctxt "@title"
msgid "Appearance"
msgstr "Megjelenés"

#: package/contents/ui/Appearance.qml:43
#, kde-format
msgid "Wallpaper type:"
msgstr "Háttérképtípus:"

#: package/contents/ui/main.qml:39
#, kde-format
msgid "Lock screen automatically:"
msgstr "Képernyő automatikus zárolása:"

#: package/contents/ui/main.qml:41
#, kde-format
msgctxt "First part of sentence \"Automatically after X minutes\""
msgid "After"
msgstr "Ennyi idő után"

#: package/contents/ui/main.qml:54
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 perc"
msgstr[1] "%1 perc"

#: package/contents/ui/main.qml:69
#, kde-format
msgctxt "@option:check"
msgid "After waking from sleep"
msgstr "Alvó állapotból visszatéréskor"

#: package/contents/ui/main.qml:84
#, kde-format
msgctxt "@label:spinbox"
msgid "Allow unlocking without password for:"
msgstr "Jelszó nélküli feloldás időkorlátja:"

#: package/contents/ui/main.qml:89
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 másodperc"
msgstr[1] "%1 másodperc"

#: package/contents/ui/main.qml:108
#, kde-format
msgid "Keyboard shortcut:"
msgstr "Billentyűparancs:"

#: package/contents/ui/main.qml:123
#, kde-format
msgid "Appearance:"
msgstr "Megjelenés:"

#: package/contents/ui/main.qml:124
#, kde-format
msgctxt "@action:button"
msgid "Configure..."
msgstr "Beállítás…"